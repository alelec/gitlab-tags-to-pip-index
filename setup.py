from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='gitlab-tags-to-pip-index',
    use_scm_version=True,  # This will generate the version number from git tags

    description='Utility for use in gitlab ci to create python repo web pages from project tags',
    long_description=long_description,
    url='https://gitlab.com/alelec/gitlab-tags-to-pip-index',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['gitlab_tags_to_pip_index'],
    setup_requires=['setuptools_scm'],
    install_requires=['requests', 'mako'],
    entry_points={
        'console_scripts': [
            'gitlab_tags_to_pip_index=gitlab_tags_to_pip_index:main',
        ],
    },
)
